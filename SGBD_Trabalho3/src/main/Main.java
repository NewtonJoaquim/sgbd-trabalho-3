package main;

import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.SingleSelectionModel;

import controle.Item;
import controle.LockTable;
import controle.Lock_Manager;
import enums.Bloqueios;
import enums.Estados;
import grafo.Grafo;
import grafo.Transacao;

public class Main {
	
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		Tr_Manager tm = new Tr_Manager();
		tratador_de_entradas te = new tratador_de_entradas();
		Lock_Manager lm = new Lock_Manager();
		LockTable lt = lm.getLockTable();
		Grafo grafo = new Grafo();
		String historia;
		int it = 0;
		//ArrayList<String> cmdHistoria;
		//ArrayList<Item> listaItens = new ArrayList<Item>();
		
		//Recebendo historia
		System.out.println("Entre com a historia: ");
		historia = keyboard.nextLine();
		
		System.out.println("aff:" +te.getListaDeComandos(historia).size());
		
		ArrayList<String> cmdHistoria = te.getListaDeComandos(historia);
		ArrayList<Item> listaItens = te.getListaDeItens(cmdHistoria);
		
		
		//ArrayList<String> test = te.getListaDeComandos(historia);
		//System.out.println("tamanho vetor:"+te.getListaDeComandos(historia).size());
		//System.out.println("tamanho vetor:"+test.size());
		
		//for(String next: cmdHistoria) {
			//System.out.println(next);
		//}
		
		//condição de desbloqueio
		boolean unlock;
		
		//Fase de Crescimento
			//Lendo historia
			System.out.println(cmdHistoria.size());
			System.out.println("Iniciando fase de crescimento.\n");
			while (it < cmdHistoria.size()) {
				/*unlock = false;
				
				
				Item it = listaItens.get(0);
				
				for(int i= 0;i<cmdHistoria.size();i+=2){
					String aux = cmdHistoria.get(i);
					String itemID = cmdHistoria.get(i+1);
					if((aux == "r" && itemID == it.getId())||(aux == "w" && itemID == it.getId())||(aux=="C" && itemID==it.getId())){
						unlock = false;
						break;
					}
					else{
						unlock = true;
					}
				}
				
				if(unlock = true){
					String trID = cmdHistoria.get(0);
					String itemID = cmdHistoria.get(1);
					lm.U(grafo.acharTransacao(trID), itemID);
					listaItens.remove(0);
				}*/
				
				
				String cmd = cmdHistoria.get(it);
				System.out.println("item sendo lido:"+cmd+"\n");
				
				if (cmd == "BT") {
					String trID = cmdHistoria.get(it+1);
					tm.trBegin(trID);		
					System.out.println("Transacao"+trID+"criada com sucesso.\n");
				}
				else if (cmd == "C") {
					String trID = cmdHistoria.get(it+1);
					tm.trTerminate(trID);
					System.out.println("Transacao"+trID+"commitada com sucesso.\n");
				}
				else if (cmd == "r") {
					String trID = cmdHistoria.get(it+1);
					Transacao tr = grafo.acharTransacao(trID);
					String itemID = cmdHistoria.get(it+2);
					Item item = lm.getItem(itemID);
					System.out.println("Realizando leitura no item"+itemID+"com a transacao"+trID+"\n");
					
					if ((tr.getEstadoAtual() == Estados.INICIADA) || (tr.getEstadoAtual() == Estados.ATIVA)) {
						if (lm.LS(grafo.acharTransacao(trID), itemID)) {		//tr envia solicita��o para ls em itemID
							tm.trAccess(trID);
						}
						else {
							if (tr.getTimestamp() > item.getWts()) {
								item.adicionarEspera(tr, Bloqueios.LS);
							}
							else {
								Transacao trRollback = grafo.acharTransacao(item.getWts());
								lm.U(trRollback, itemID);
								tm.trRollback(trRollback.getTrID());
								//item.setWts(0);
								
								if ((lm).LS(grafo.acharTransacao(trID), itemID)) {		//tr envia solicita��o para ls em itemID
									tm.trAccess(trID);
								}
								else {
									item.adicionarEspera(tr, Bloqueios.LS);
								}
							}
						}
					}					
				}
				else if (cmd == "w") {
					String trID = cmdHistoria.get(it+1);
					Transacao tr = grafo.acharTransacao(trID);
					String itemID = cmdHistoria.get(it+2);
					Item item = lm.getItem(itemID);
					System.out.println("realizando escrita no item"+itemID+"com a transacao"+trID+"\n");
					
					if ((tr.getEstadoAtual() == Estados.INICIADA) || (tr.getEstadoAtual() == Estados.ATIVA)) {
						if (lm.LX(grafo.acharTransacao(trID), itemID)) { 	//tr envia solicita��o para lx em itemID
							tm.trAccess(trID);
						}
					}
				
				System.out.println("Grafo de Estados:");
				grafo.listarTransacoes();
				it++;
			}
			
			
		//Fase de Encolhimento
			//Lendo historia
			System.out.println("Iniciando fase de encolhimento.\n");
			
			//para cada item
	}
}}
