package main;

import enums.Estados;
import grafo.Estado;
import grafo.Grafo;
import grafo.Transacao;

public class Tr_Manager {
	private  Grafo grafo;
	public String historia;
	
	public void start () {
		grafo = new Grafo();
	}
	public void start(Grafo grafo) {
		this.grafo = grafo;
	}
	
	
	
	//Operacoes
	public void trBegin (String id) {
		Transacao novaTransacao = new Transacao(id);
		novaTransacao.setEstadoAtual(Estados.INICIADA);
		
		grafo.getListaTransacoes().add(novaTransacao);
		Estado aux = grafo.acharNode(novaTransacao.getEstadoAtual());
		aux.adicionarTransacao(novaTransacao);
	}

	public boolean trAccess (String trID) {
		try{
			Transacao tr = grafo.acharTransacao(trID);
			if (tr.getEstadoAtual() == Estados.INICIADA || tr.getEstadoAtual() == Estados.ATIVA) {
				grafo.moverTransacao(tr, Estados.ATIVA);
				return true;
			}
		}catch (NullPointerException e) {
			//tratamento da exce��o
		}
		return false;
	}
	
	public void trTerminate (String trID) {
		try{
			Transacao tr = grafo.acharTransacao(trID);
			if (tr.getEstadoAtual() == Estados.ATIVA) {
				grafo.moverTransacao(tr, Estados.EFETIVACAO);
			}
		} catch (NullPointerException e) {
			//tratamento da exce��o
		}
	}
	
	public void trRollback (String trID) {
		try{
			Transacao tr = grafo.acharTransacao(trID);
			if (tr.getEstadoAtual() == Estados.ATIVA || tr.getEstadoAtual() == Estados.EFETIVACAO) {
				grafo.moverTransacao(tr, Estados.CANCELAMENTO);
			}
		} catch (NullPointerException e) {
			//tratamento da exce��o
		}
	}
	
	public void trCommit (String trID) {
		try{
			Transacao tr = grafo.acharTransacao(trID);
			if (tr.getEstadoAtual() == Estados.EFETIVACAO) {
				grafo.moverTransacao(tr, Estados.EFETIVADA);
			}
		} catch (NullPointerException e) {
			//tratamento da exce��o
		}
	}
	
	public void trFinish (String trID) {
		try{
			Transacao tr = grafo.acharTransacao(trID);
			if (tr.getEstadoAtual() == Estados.CANCELAMENTO || tr.getEstadoAtual() == Estados.EFETIVADA) {
				grafo.moverTransacao(tr, Estados.FINALIZADA);
			}
		} catch (NullPointerException e) {
			//tratamento da exce��o
		}
	}
}
