package controle;

import java.util.LinkedList;

import enums.Bloqueios;
import grafo.Transacao;

public class WaitQ {
	private LinkedList<Transacao> listaTransacoes = new LinkedList<Transacao>();
	private LinkedList<Bloqueios> listaBloqueios = new LinkedList<Bloqueios>();
	
	
	//Construtor
		
	public WaitQ() {}

	//Getters & Setters
	public LinkedList<Transacao> getListaTransacoes() {
		return listaTransacoes;
	}
	
	public LinkedList<Bloqueios> getListaBloqueios () {
		return listaBloqueios;
	}
	
	
	//M�todos
	//inserir, remover
	
	public void inserir (Transacao tr, Bloqueios tipoBloqueio) {
		listaTransacoes.addLast(tr);
		listaBloqueios.addLast(tipoBloqueio);
	}
	
	public Transacao getProxTransacao () {
		return listaTransacoes.getFirst();
	}
	
	public Bloqueios getProxBloqueio () {
		return listaBloqueios.getFirst();
	}
	
	public void remover () {
		listaTransacoes.removeFirst();
		listaBloqueios.removeFirst();
	}
	
	public boolean isEmpty() {
		if ((listaTransacoes.isEmpty()) && (listaBloqueios.isEmpty())) {
			return true;
		}
		else return false;
	}
}
