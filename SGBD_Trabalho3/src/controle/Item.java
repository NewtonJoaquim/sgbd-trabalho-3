package controle;

import enums.Bloqueios;
import grafo.Transacao;

public class Item {
	public String id;
	public WaitQ listaEspera = new WaitQ();
	public Bloqueios bloqueioAtual;
	public Transacao transacaoBloqueante;
	public int wts = 0;
	public int rts = 0;
	

	public Transacao getTransacaoBloqueante() {
		return transacaoBloqueante;
	}

	public void setTransacaoBloqueante(Transacao transacaoBloqueante) {
		this.transacaoBloqueante = transacaoBloqueante;
	}

	//Construtor
	public Item (String id) {
		this.id = id;
	}	
	
	public int getRts() {
		return rts;
	}
	public void setRts(int rts) {
		this.rts = rts;
	}
	public int getWts() {
		return wts;
	}
	public void setWts(int wts) {
		this.wts = wts;
	}	
	public String getId() {
		return id;
	}
	public WaitQ getListaEspera() {
		return listaEspera;
	}
	public Bloqueios getBloqueioAtual () {
		return bloqueioAtual;
	}
	public void setBloqueioAtual (Bloqueios bloqueio) {
		this.bloqueioAtual = bloqueio;
	}
	
	
	//M�todos
	public boolean isBloqueado() {
		if (bloqueioAtual == Bloqueios.U) {
			return true;
		} return false;
	}
	
	public boolean isBloqueadoParaLeitura () {
		if ((bloqueioAtual == Bloqueios.U) || (bloqueioAtual == Bloqueios.LS)) {
			return false;
		}else return true;
	}
	
	public boolean isBloqueadoParaEscrita() {
		if (bloqueioAtual == Bloqueios.LX) {
			return true;
		}else return false;
	}
	
	public void desbloquear () {
		if (listaEspera.isEmpty()) {
			this.bloqueioAtual = Bloqueios.U;
		}
		else {
			
		}
	}
	
	public void adicionarEspera (Transacao tr, Bloqueios tipoBloqueio) {
		listaEspera.inserir(tr, tipoBloqueio);
	}
	
	public void adicionarBloqueioDeEscrita (Transacao tr) {
		/*if (!this.isBloqueado()) {
			if (this.listaEspera.isEmpty()) {
				this.bloqueioAtual = Bloqueios.LX;
				transacaoBloqueante = tr;
			}
			else {
				this.bloqueioAtual = listaEspera.getProxBloqueio();		//e se o pr�ximo bloqueio da lista n�o for lx?
				this.transacaoBloqueante = listaEspera.getProxTransacao();
				listaEspera.remover();
			}
		}
		else {
			listaEspera.getListaTransacoes().add(tr);
			listaEspera.getListaBloqueios().add(Bloqueios.LX);
		}*/
	}
}
