package controle;

import java.util.ArrayList;

import enums.Bloqueios;
import grafo.Transacao;

public class Lock_Manager {
	public LockTable lockTable = new LockTable();
	public ArrayList<Item> listaItens = new ArrayList<Item>();
	
	public Lock_Manager() {}
	
	public LockTable getLockTable() {
		return lockTable;
	}
	
	public Item getItem (String itemID) {
		for (Item aux: listaItens) {
			if (aux.getId() == itemID) {
				return aux;
			}
		}return null;
	}
	
	public boolean LS (Transacao tr, String itemID) {	//requisição de tr para LS em itemID
		Item item = getItem(itemID);
			
		if (!item.isBloqueadoParaLeitura()) {
			tr.addItemBloqueado(item);
			item.setBloqueioAtual(Bloqueios.LS);
			lockTable.transacoesAtivas.add(tr);
			item.setRts(tr.getTimestamp());
			
			if (!lockTable.isTrNaTabela(tr)) {
				lockTable.adicionarTr(tr);
			}
			return true;
		}
		else {
			return false;
			/*if (tr.getTimestamp() > item.getTransacaoBloqueante().getTimestamp()){
				item.adicionarEspera(tr, Bloqueios.LS);	
				return false;
			}
			else {
				//rollback na transacao bloqueante
				item.getTransacaoBloqueante().rmvItemBloqueado(item);
				
				tr.addItemBloqueado(item);
				item.setBloqueioAtual(Bloqueios.LS);
				lockTable.transacoesAtivas.add(tr);
				if (!lockTable.isTrNaTabela(tr)) {
					lockTable.adicionarTr(tr);
				}
				return true;
			}*/
		}
	}
	
	public boolean LX (Transacao tr, String itemID) {	//requisição de tr para LX em itemID
		Item item = getItem(itemID);
		
		if (!item.isBloqueadoParaEscrita()) {
			if (tr.temLS(itemID)) {
				tr.addItemBloqueado(item);
				item.setBloqueioAtual(Bloqueios.LX);
				item.setTransacaoBloqueante(tr);
				item.setWts(tr.getTimestamp());
			}	
			return true;
		}
		else {
			item.adicionarEspera(tr, Bloqueios.LX);
			return false;
		}
	}
	
	public void U (Transacao tr, String itemID) {
		if (tr.estaBloqueando(itemID)) {
			Item item = getItem(itemID);
			tr.rmvItemBloqueado(item);
		}
	}


}
