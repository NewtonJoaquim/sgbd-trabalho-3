package controle;

import enums.Bloqueios;
import grafo.Transacao;

public class Bloqueio {
	public Transacao transacao;
	public Bloqueios tipo;
	
	public Bloqueio (Transacao tr) {
		this.transacao = tr;
	}

	public Transacao getTransacao() {
		return transacao;
	}

	public Bloqueios getTipo() {
		return tipo;
	}

	public void setTipo(Bloqueios tipo) {
		this.tipo = tipo;
	}
}
