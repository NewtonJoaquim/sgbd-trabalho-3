package controle;

import java.util.ArrayList;

import grafo.Transacao;

public class LockTable {
	public ArrayList<Transacao> transacoesAtivas = new ArrayList<Transacao>();
	public ArrayList<Transacao> transacoesInativas = new ArrayList<Transacao>();
	
	public LockTable() {}
	
	public void adicionarTr (Transacao tr) {
		transacoesAtivas.add(tr);
	}
	
	public void removerTr (Transacao tr) {
		for (Transacao aux: transacoesAtivas) {
			if (aux.equals(tr)) {
				transacoesAtivas.remove(aux);
			}
		}
	}
	
	public void removerTr (String trID) {
		for (Transacao aux: transacoesAtivas) {
			if (aux.getTrID() == trID) {
				transacoesAtivas.remove(aux);
			}
		}
	}
	
	public Transacao getTr (String id) {
		for (Transacao aux: transacoesAtivas) {
			if (aux.getTrID() == id) {
				return aux;
			}
		}return null;
	}
	
	public boolean isTrNaTabela (Transacao tr) {
		for (Transacao aux: transacoesAtivas) {
			if (aux == tr) {
				return true;
			}
		}return false;
	}
}
