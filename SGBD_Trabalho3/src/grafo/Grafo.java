package grafo;

import java.util.ArrayList;

import enums.Estados;

public class Grafo {
	//lista de n�s; lista de trs
	private ArrayList<Transacao> listaTransacoes = new ArrayList<Transacao>();
	private ArrayList<Estado> listaEstados = new ArrayList<Estado>();
	
	//Construtor
	public Grafo () {
		listaEstados.add(new Estado(Estados.INICIADA));
		listaEstados.add(new Estado(Estados.ATIVA));
		listaEstados.add(new Estado(Estados.EFETIVACAO));
		listaEstados.add(new Estado(Estados.EFETIVADA));
		listaEstados.add(new Estado(Estados.CANCELAMENTO));
		listaEstados.add(new Estado(Estados.FINALIZADA));
	}
	
	//Getters & Setters
	public ArrayList<Transacao> getListaTransacoes () {
		return listaTransacoes;
	}
	
	public ArrayList<Estado> getListaEstados () {
		return listaEstados;
	}
	
	
	//M�todos
	
	public void consultarNode (Estado node) {
		for (Estado aux: listaEstados) {
			if (aux == node) {
				aux.listarTransacoes();
			}
		}
	}
	
	public Estado acharNode (Estados estado) {
		for(Estado aux: listaEstados) {
			if (aux.getEstadoNode() == estado) {
				return aux;
			}
		}return null;
	}
	
	public void listarTransacoes () {
		for (Transacao aux: listaTransacoes) {
			System.out.println("Transacao id = " + aux.getTrID() + " - " + aux.getEstadoAtual() + "\n");
		}
	}
	
	public void moverTransacao (Transacao tr, Estados novoEstado) {
		Estado atual = acharNode(tr.getEstadoAtual());
		Estado novo = acharNode(novoEstado); 
		
		if (atual.contains(tr)){
			novo.adicionarTransacao(tr);
			atual.removerTransacao(tr);
		}
		else 
			System.out.println("Erro na operacao\n");
	}
	
	public Transacao acharTransacao (String id) {
		for (Transacao aux: listaTransacoes) {
			if (aux.getTrID() == id) {
				return aux;
			}
		}return null;
	}
	
	public Transacao acharTransacao (int ts) {
		for (Transacao aux: listaTransacoes) {
			if (aux.getTimestamp() == ts) {
				return aux;
			}
		}return null;
	}
}
