package grafo;


import java.util.ArrayList;
import java.util.LinkedList;

import controle.Item;
import enums.Bloqueios;
import enums.Estados;

public class Transacao {
	public Estados estadoAtual;
	public String trID;
	private static int timestamp;
	public ArrayList<Item> itensBloqueados = new ArrayList<Item>();

	
	public Transacao (String id) {
		timestamp = timestamp+1;
		this.trID = id;
		System.out.println("Transacao criada com sucesso! ID: "+trID);
	}

	public Estados getEstadoAtual() {
		return estadoAtual;
	}

	public void setEstadoAtual(Estados estadoAtual) {
		this.estadoAtual = estadoAtual;
	}

	public String getTrID() {
		return trID;
	}

	public int getTimestamp () {
		return timestamp; 
	}

	public void addItemBloqueado (Item item) {
		itensBloqueados.add(item);
	}
	
	public void rmvItemBloqueado (Item item) {
		for (Item aux: itensBloqueados) {
			if (aux.equals(item)) {
				itensBloqueados.remove(aux);
			}
		}
	}
	
	public boolean estaBloqueando (String itemID) {
		for (Item aux: itensBloqueados) {
			if (aux.getId() == itemID) {
				return true;
			}
		}return false;
	}
	
	public boolean estaBloqueando (Item item) {
		for (Item aux: itensBloqueados) {
			if (aux.equals(item)) {
				return true;
			}
		}return false;
	}
	
	public boolean temLX (String itemID) {
		for (Item aux: itensBloqueados) {
			if (aux.getBloqueioAtual() == Bloqueios.LX) {
				return true;
			}
		}return false;
	}
	
	public boolean temLS (String itemID) {
		for (Item aux: itensBloqueados) {
			if (aux.getBloqueioAtual() == Bloqueios.LS) {
				return true;
			}
		}return false;
	}
	
	
	public ArrayList<Item> getItensBloqueados () {
		return itensBloqueados;
	}
}
