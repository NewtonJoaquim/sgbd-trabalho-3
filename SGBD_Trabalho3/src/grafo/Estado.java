package grafo;

import java.util.ArrayList;

import enums.Estados;

public class Estado {
	public ArrayList<Transacao> listaTransacoes = new ArrayList<Transacao>();
	public Estados estado;
	
	//Construtor
	public Estado (Estados estado) {
		this.estado = estado;
	}
	
	
	//Getters & Setters
	public ArrayList<Transacao> getTr() {
		return listaTransacoes;
	}

	public Estados getEstadoNode() {
		return estado;
	}
	
	
	//Metodos
	public void adicionarTransacao (Transacao tr) {		//adiciona transa��o ao n�
		listaTransacoes.add(tr);
	}
	
	public void removerTransacao (Transacao tr) {		//remove transa��o do n�
		listaTransacoes.remove(tr);
	}
	
	public boolean contains (Transacao tr2) {		//verifica se transa��o est� no n�
		if (tr2.getEstadoAtual() == this.estado) {
			if (listaTransacoes.contains(tr2)) {
				return true;
			}
		}
		return false;
	}
	
	public void listarTransacoes () {
		for (Transacao aux: listaTransacoes){
			System.out.println("Transacao " + aux.getTrID() + " - " + aux.getEstadoAtual());
		}
	}
}
